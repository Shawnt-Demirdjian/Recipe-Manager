const mongoose = require('mongoose');

const mongoHost = process.env.MONGO_HOST;
const mongoPort = process.env.MONGO_PORT;
const authDB = process.env.AUTH_DB;

// Connect to MongoDB
mongoose.connect(`mongodb://${mongoHost}:${mongoPort}`, {
  auth: {
    authSource: authDB,
  },
  dbName: process.env.DB_NAME,
  user: process.env.DB_USER,
  pass: process.env.DB_PASS,
});

// Create GridFS connection for Images
const ImagesBucket = new mongoose.mongo.GridFSBucket(mongoose.connection, {
  bucketName: 'images',
});

export default ImagesBucket;
