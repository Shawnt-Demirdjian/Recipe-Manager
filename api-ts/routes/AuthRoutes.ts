import { Request, Response } from 'express';
import IAuthRequest from '../models/AuthRequest';
import { AuthenticateToken, GenerateAccessToken } from '../functions/JwtValidataionUtils';

const bcrypt = require('bcrypt');
const express = require('express');

const router = express.Router();

const USERNAME = process.env.JWT_USER!;
const HASHED_PASS = process.env.JWT_PASS!;

// Authenticate User
router.post('/', (req: Request, res: Response) => {
  const request: IAuthRequest = req.body;
  if (USERNAME !== request.username) { res.sendStatus(401); return; }

  // Used to Generate Hash
  // const salt = bcrypt.genSaltSync(10);
  // const hash = bcrypt.hashSync(request.password, salt);

  const result: boolean = bcrypt.compareSync(request.password, HASHED_PASS);
  if (!result) { res.sendStatus(401); return; }
  res.json({ jwtToken: GenerateAccessToken(USERNAME) });
});

// Test Token
router.get('/', (req: Request, res: Response) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];
  res.send(AuthenticateToken(token!));
});

module.exports = router;
