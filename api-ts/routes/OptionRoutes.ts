import { Request, Response } from 'express';
import { ValidCategories } from '../models/Category';
import { ValidCookingMethods } from '../models/CookingMethod';

const express = require('express');

const router = express.Router();

// Get all Reviews for a Recipe
router.get('/categories', (req: Request, res: Response) => {
  res.json(ValidCategories());
});

// Get a Review for a Recipe
router.get('/cooking-methods', (req: Request, res: Response) => {
  res.json(ValidCookingMethods());
});

module.exports = router;
