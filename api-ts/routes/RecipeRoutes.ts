import { Request, Response } from 'express';
import { IRecipe, Recipe } from '../models/Recipe';
import SearchOptions from '../models/SearchOptions';
import ImagesBucket from '../connections/MongoConnection';

const mongoose = require('mongoose');
const express = require('express');

const router = express.Router();

// Get all Recipes
router.get('/', (req: Request, res: Response) => {
  const promise = Recipe.find({}).exec();
  promise.then((docs: any) => {
    res.json(docs);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Get a Recipe
router.get('/:id', (req: Request, res: Response) => {
  // TODO: Check if Exists first
  const idToGet: string = req.params.id;
  const promise = Recipe.findById(idToGet).exec();
  promise.then((docs: any) => {
    res.json(docs);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Get all Recipes that match the search critera
router.post('/search', (req: Request, res: Response) => {
  const searchOptions: SearchOptions = SearchOptions.from(req.body);
  const promise = Recipe.find(
    searchOptions.AssembleFilter(),
    null,
    searchOptions.AssembleOptions(),
  ).exec();
  promise.then((doc: any) => {
    res.status(200).json(doc);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Create a Recipe
router.post('/', (req: Request, res: Response) => {
  const requestRecipe: IRecipe = req.body;
  const recipeToSave = new Recipe(requestRecipe);
  const promise = recipeToSave.save();
  promise.then((doc: any) => {
    res.status(201).json(doc);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Update a Recipe
router.put('/:id', (req: Request, res: Response) => {
  // TODO: Check if Exists first
  const idToUpdate: string = req.params.id;
  const requestRecipe: IRecipe = req.body;

  const promise = Recipe.findByIdAndUpdate({ _id: idToUpdate }, requestRecipe).exec();
  promise.then((doc: any) => {
    res.status(200).json(doc);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Delete a Recipe
router.delete('/:id', async (req: Request, res: Response) => {
  const idToDelete: string = req.params.id;
  let imageIds :String[] = [];
  const getPromise = Recipe.findById(idToDelete).exec();
  getPromise.then((doc: any) => {
    imageIds = doc.images;
  });
  getPromise.catch((err: Error) => {
    res.status(500).send(err);
  });
  await getPromise;

  const deletePromise = Recipe.deleteOne({ _id: idToDelete }).exec();
  deletePromise.then((doc: any) => {
    // Delete any assocaited images
    imageIds.forEach((imageId: String) => {
      ImagesBucket.delete(mongoose.Types.ObjectId(imageId));
    });
    res.status(200).json(doc);
  });
  deletePromise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

module.exports = router;
