import { Request, Response } from 'express';
import { Recipe } from '../models/Recipe';
import ImagesBucket from '../connections/MongoConnection';

const mongoose = require('mongoose');

const multer = require('multer');
const express = require('express');

const storage = multer.memoryStorage();
const upload = multer({ storage });

const router = express.Router();

// Upload an Image
router.post('/:recipeId/images', upload.single('image'), (req: Request, res: Response) => {
  // TODO: check if recipe exists first
  const uploadedFile: Express.Multer.File = req.file!;
  const imageBucketStream = ImagesBucket.openUploadStream(uploadedFile.originalname);
  const newFileId: string = imageBucketStream.id;
  const { recipeId } = req.params;

  imageBucketStream.write(uploadedFile.buffer);
  imageBucketStream.end();

  // associate image with Recipe
  const promise = Recipe.findByIdAndUpdate(recipeId, { $push: { images: newFileId } }).exec();
  promise.then((docs: any) => {
    res.sendStatus(201);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

// Get an Image
router.get('/:recipeId/images/:imageId', (req: Request, res: Response) => {
  // TODO: check if recipe exists first
  const { recipeId, imageId } = req.params;
  const imageBucketStream = ImagesBucket.openDownloadStream(mongoose.Types.ObjectId(imageId));
  imageBucketStream.s.files.findOne({ _id: mongoose.Types.ObjectId(imageId) })
    .then((file: any) => {
      res.type('image/*');
      res.setHeader('X-Image-Name', file.filename);
      res.setHeader('Access-Control-Expose-Headers', 'X-Image-Name');
      imageBucketStream.pipe(res);
    })
    .catch((err: any) => {
      res.status(500).send(err);
    });
});

// Delete an Image
router.delete('/:recipeId/images/:imageId', (req: Request, res: Response) => {
  // TODO: check if recipe exists first
  // TODO: check if image exists first
  const { recipeId, imageId } = req.params;
  ImagesBucket.delete(mongoose.Types.ObjectId(imageId));

  // remove image association with Recipe
  const promise = Recipe.findByIdAndUpdate(recipeId, { $pull: { images: imageId } }).exec();
  promise.then((docs: any) => {
    res.sendStatus(200);
  });
  promise.catch((err: Error) => {
    res.status(500).send(err);
  });
});

module.exports = router;
