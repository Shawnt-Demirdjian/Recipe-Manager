enum Category {
  CARB_SIDE = 'CARB_SIDE',
  VEGGIE_SIDE = 'VEGGIE_SIDE',
  ENTREE = 'ENTREE',
  SOUP = 'SOUP',
  SAUCE = 'SAUCE',
  DESSERT = 'DESSERT',
  APPETIZER = 'APPETIZER',
  INVALID = 'INVALID',
}

function ValidCategories(): Category[] {
  return [
    Category.CARB_SIDE,
    Category.VEGGIE_SIDE,
    Category.ENTREE,
    Category.SOUP,
    Category.SAUCE,
    Category.DESSERT,
    Category.APPETIZER,
  ];
}

export { Category, ValidCategories };
