import { Request, Response, NextFunction } from 'express';
import { AuthenticateToken } from '../functions/JwtValidataionUtils';

function AuthenticateTokenMiddleware(req: Request, res: Response, next: NextFunction) {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];

  if (token == null || !AuthenticateToken(token)) { res.sendStatus(401); return; }

  next();
}

module.exports = AuthenticateTokenMiddleware;
