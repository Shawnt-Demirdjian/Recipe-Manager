package com.demirdjian.recipemanager.db;

import java.util.Optional;

import com.demirdjian.recipemanager.models.Review;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ReviewRepository extends MongoRepository<Review, String>, ReviewRepositoryCustom {

    /**
     * Find a review by it's ID.
     * 
     * @param id
     * @return Optional <Review>
     */
    public Optional<Review> findById(String id);
}
