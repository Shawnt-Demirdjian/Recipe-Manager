package com.demirdjian.recipemanager.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.demirdjian.recipemanager.models.Category;
import com.demirdjian.recipemanager.models.CookingMethod;
import com.demirdjian.recipemanager.models.Recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.TextCriteria;

public class RecipeRepositoryImpl implements RecipeRepositoryCustom {

    @Autowired
    private MongoTemplate template;

    private static final ArrayList<String> VALID_SORT_FIELDS = new ArrayList<>(Arrays.asList("creationDate", "title"));

    @Override
    public List<Recipe> search(String queryText, Category category, CookingMethod cookingMethod, Date createdBefore,
            Date createdAfter, Integer limit, String sortBy, Direction order) {
        ArrayList<AggregationOperation> operations = new ArrayList<>();

        // Add TextQuery if queryText is provided
        if (queryText != null) {
            // Split queryText on whitespace
            String[] queryArr = queryText.split("\\s+");

            // Full Text Search on now split terms
            TextCriteria criteria = TextCriteria.forDefaultLanguage().caseSensitive(false).matchingAny(queryArr);
            operations.add(Aggregation.match(criteria));
        }

        // Add criteria for category and cooking method, if provided
        if (category != null) {
            operations.add(Aggregation.match(Criteria.where("category").is(category)));
        }
        if (cookingMethod != null) {
            operations.add(Aggregation.match(Criteria.where("cookingMethod").is(cookingMethod)));
        }
        if (createdBefore != null) {
            operations.add(Aggregation.match(Criteria.where("creationDate").lte(createdBefore)));
        }
        if (createdAfter != null) {
            operations.add(Aggregation.match(Criteria.where("creationDate").gte(createdAfter)));
        }

        // Default to descending order
        if (order == null) {
            order = Direction.DESC;
        }

        // Sort must be immediately before limit
        // Add sort, if given is valid. Default to creationDate
        if (sortBy != null && VALID_SORT_FIELDS.contains(sortBy)) {
            operations.add(Aggregation.sort(order, sortBy));
        } else {
            operations.add(Aggregation.sort(order, "creationDate"));
        }

        if (limit != null) {
            operations.add(Aggregation.limit(limit));
        }

        Aggregation aggregation = Aggregation.newAggregation(operations);
        return template.aggregate(aggregation, template.getCollectionName(Recipe.class), Recipe.class)
                .getMappedResults();
    }

}
