package com.demirdjian.recipemanager.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = ImageValidator.class)
@Target({ ElementType.FIELD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ImageConstraint {

	/**
	 * Returns the error message. Defaults to "Invalid Image Type.".
	 * 
	 * @return String
	 */
	String message() default "Invalid Image Type.";

	/**
	 * Defines targeted groups.
	 * 
	 * @return Class<?>[]
	 */
	Class<?>[] groups() default {};

	/**
	 * For extensibility purposes.
	 * 
	 * @return Class<? extends Payload>
	 */
	Class<? extends Payload>[] payload() default {};
}
