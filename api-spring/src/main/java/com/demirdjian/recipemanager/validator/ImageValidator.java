package com.demirdjian.recipemanager.validator;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

public class ImageValidator implements ConstraintValidator<ImageConstraint, MultipartFile> {

	private static final List<MediaType> ALLOWED_IMAGE_TYPES = List.of(MediaType.IMAGE_JPEG);

	@Override
	public void initialize(ImageConstraint image) {
		// initializes validator before running isValid
	}

	@Override
	public boolean isValid(MultipartFile image, ConstraintValidatorContext cxt) {
		String reqTypeStr = image.getContentType();

		if (reqTypeStr != null) {
			try {
				MediaType reqType = MediaType.parseMediaType(reqTypeStr);
				return ALLOWED_IMAGE_TYPES.contains(reqType);
			} catch (InvalidMediaTypeException e) {
				return false;
			}
		}
		return false;
	}

}
