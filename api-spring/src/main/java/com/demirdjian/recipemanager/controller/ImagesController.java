package com.demirdjian.recipemanager.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import com.demirdjian.recipemanager.db.RecipeRepository;
import com.demirdjian.recipemanager.error.CustomGlobalExceptionHandler;
import com.demirdjian.recipemanager.models.Recipe;
import com.demirdjian.recipemanager.validator.ImageConstraint;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.model.GridFSFile;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Validated
@RestController
@RequestMapping("/api")
public class ImagesController {

	@Autowired
	private GridFsTemplate gridFsTemplate;
	@Autowired
	private RecipeRepository recipeRepository;
	private static final Logger RM_LOGGER = LoggerFactory.getLogger(ImagesController.class);

	private static final int MAX_IMAGES = 3;

	/**
	 * Returns the Image with the associated ID.
	 *
	 * @param id           The image ID
	 * @param httpResponse
	 * 
	 * @return ResponseEntity<InputStreamResource> The image associated with the
	 *         given ID. Or null.
	 */
	@GetMapping("/images/{id}")
	public ResponseEntity<InputStreamResource> getImage(@PathVariable("id") @NotBlank String id,
			HttpServletResponse httpResponse) {
		GridFSFile imageFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(id)));

		if (imageFile == null) {
			// no image was found
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
			RM_LOGGER.debug("No Image Found: {}.", id);
			return null;
		}
		GridFsResource imageResource = gridFsTemplate.getResource(imageFile);

		try {
			return ResponseEntity.ok().contentLength(imageFile.getLength())
					.contentType(MediaType.parseMediaType((String) imageFile.getMetadata().get("_contentType")))
					.body(new InputStreamResource(imageResource.getInputStream()));
		} catch (IllegalStateException | IOException e) {
			RM_LOGGER.error("Error getting Image with ID: {}", id, e);
			httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return null;
		}

	}

	/**
	 * Uploads an image and associates it with the Recipe with the given ID.
	 *
	 * @param file         The image being uploaded
	 * @param recipeId     The ID of the recipe associated with this image
	 * @param httpResponse
	 * 
	 * @return ResponseEntity<Object>, ID of new image created
	 */
	@PostMapping("/recipes/{recipe-id}/images")
	public ResponseEntity<Object> uploadImage(@RequestParam("file") @Valid @ImageConstraint MultipartFile file,
			@PathVariable("recipe-id") @NotBlank String recipeId, HttpServletResponse httpResponse) {

		Optional<Recipe> recipeOpt = recipeRepository.findById(recipeId);

		// Verify recipe exists
		if (recipeOpt.isPresent()) {
			try {
				// limit 3 images per recipe
				Recipe recipe = recipeOpt.get();
				List<String> imageList = recipe.getImages();
				if (imageList.size() >= MAX_IMAGES) {
					return CustomGlobalExceptionHandler.createCustomErrorResponseBody(
							Arrays.asList("This recipe has hit the max number of images: " + MAX_IMAGES),
							HttpStatus.BAD_REQUEST);
				}

				// add image to DB
				DBObject metadata = new BasicDBObject();
				metadata.put("fileSize", file.getSize());
				ObjectId fileID = gridFsTemplate.store(file.getInputStream(), file.getOriginalFilename(),
						file.getContentType(), metadata);

				// associate image with recipe
				imageList.add(fileID.toString());
				recipe.setImages(imageList);
				recipeRepository.save(recipe);

				// Create and return good response
				Map<String, Object> body = new LinkedHashMap<>();
				body.put("imageId", fileID.toString());
				return new ResponseEntity<>(body, HttpStatus.OK);

			} catch (IOException e) {
				RM_LOGGER.error("Error uploading image to Recipe: {}", recipeId, e);
				httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} else {
			RM_LOGGER.debug("No Recipe Found: {}.", recipeId);
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
		return null;
	}

	/**
	 * Deletes an image and removes it from the associated recipe.
	 *
	 * @param recipeId     The ID of the recipe associated with this image
	 * @param imageId      The ID of the image to delete
	 * @param httpResponse
	 */
	@DeleteMapping("/recipes/{recipe-id}/images/{image-id}")
	public void deleteImage(@PathVariable("recipe-id") @NotBlank String recipeId,
			@PathVariable("image-id") @NotBlank String imageId, HttpServletResponse httpResponse) {

		Optional<Recipe> recipeOpt = recipeRepository.findById(recipeId);
		GridFSFile imageFile = gridFsTemplate.findOne(new Query(Criteria.where("_id").is(imageId)));

		// Verify recipe and image exist
		if (recipeOpt.isPresent() && imageFile != null) {
			// remove image from DB
			gridFsTemplate.delete(new Query(Criteria.where("_id").is(imageId)));

			// remove image ID from associated recipe
			Recipe recipe = recipeOpt.get();
			List<String> imageList = recipe.getImages();
			imageList.remove(imageId);
			recipe.setImages(imageList);

			recipeRepository.save(recipe);
		} else {
			RM_LOGGER.debug("No Recipe: {} or Image: {} Found.", recipeId, imageId);
			httpResponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
}
